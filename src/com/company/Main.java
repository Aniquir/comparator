package com.company;

public class Main {

    public static void main(String[] args) {

        Person tomek = new Person("Tomasz", "Smutek", 34);
        Person ja = new Person("Pawel", "Jezior", 26);

        tomek.toString();
        ja.toString();
        if (tomek.compareTo(ja) == 0){
            System.out.println("Osoba: " + tomek + " jest rowna z " + ja);
        }else if(tomek.compareTo(ja) < 0){
            System.out.println("Osoba: " + tomek + " jest mniejsza od " + ja);
        }else if(tomek.compareTo(ja) > 0){
            System.out.println("Osoba: " + tomek + " jest wieksza od " + ja);
        }

    }
}
