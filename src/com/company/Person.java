package com.company;

/**
 * Created by RENT on 2017-08-16.
 */
public class Person implements Comparable<Person>{

    String name;
    String surName;
    int age;

    public Person(String name, String surName, int age) {
        this.name = name;
        this.surName = surName;
        this.age = age;
    }


    @Override
    public int compareTo(Person o) {
        if (surName.length() == o.surName.length()) {
            return 0;
        } else if ( surName.length() > o.surName.length()){
            return 1;
        }else {
            return -1;
        }

    }

    @Override
    public String toString() {
        String s = name + " " + surName;
        return s;
    }
}
